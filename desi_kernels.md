Add the line
```shellscript
export DESIMODULES="/global/common/software/desi/perlmutter/desiconda/startup/modulefiles/desimodules"
```
on the .bashrc bash config file

then run 
```shellscript
${DESIMODULES}/install_jupyter_kernel.sh ###
```
with ### the version of the desi environment