# To create an environment

1) Load desi environment

`source /global/common/software/desi/desi_environment.sh`

2) Load the base conda environment

`
source ${DESICONDA}/etc/profile.d/conda.sh
conda activate
`

3) Create the conda env

`conda create -n <env_name> --copy --file <pkgs_file.txt>`

The <pkgs_file.txt> have to include ipykernel package, and the python version. Also all the packages that do you need.


4) Activate the conda env

`source activate <env_name>`


5) Install the configuration for the ipython nb 

`~/.conda/envs/<env_name>/bin/./ipython kernel install --user --name=<env_name>`


6) On /global/homes/j/jfsuarez/.local/share/jupyter/kernels/<env_name>/kernel.json
```
{
 "argv": [
  "/global/homes/j/jfsuarez/.local/share/jupyter/kernels/<env_name>/activate_<env_name>_jupyter.sh",
  "master",
  "{connection_file}"
 ],
 "display_name": "<kernel_name>",
 "language": "python"
}
```

7) On /global/homes/j/jfsuarez/.local/share/jupyter/kernels/<env_name>/activate_<env_name>_jupyter.sh
```
#!/bin/bash

version=$1
connection_file=$2

source /global/common/software/desi/desi_environment.sh ${version}
export CONDA_ENVS_PATH=/global/homes/j/jfsuarez/.conda/envs/
source activate <env_name>
export PYTHONPATH=/global/homes/j/jfsuarez/.conda/envs/<env_name>/lib/python3.9/site-packages:${PYTHONPATH}
exec python -m ipykernel -f ${connection_file}
```

8) Give the permission to the activate_<env_name>_jupyter.sh script.

```
chmod u+x activate_<env_name>_jupyter.sh
```


9) If the environment requires to use GPU with pytorch, it have to be installed the pytorch-gpu package.


The script **create_env.sh** create the env and kernel automatically.

# To remove an environment

`$ conda deactivate`

`$ conda env remove -n <env_name>`

`$ jupyter kernelspec uninstall <kernel_name>`
