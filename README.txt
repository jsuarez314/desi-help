- To compile the LSSCode from shell 

		$ module unload PrgEnv-intel
		$ module load PrgEnv-gnu
		$ make clean & make 


- To run the BSK in the cori of DESI/NERSC from nbpy

		unload = 'module unload PrgEnv-intel'
		load = 'module load PrgEnv-gnu'
		sshell = 'sh runBSK.sh .....'

		command =  unload+' && '+ load +' && '+ sshell
		! $command


- To install nbodykit for python from shell

		$ module unload PrgEnv-intel
		$ module load openmpi
		$ pip install cython mpi4py
		$ pip install nbodykit


- How to view the information from a fit file, first load the environment
		$ source /global/common/software/desi/desi_environment.sh master 

		then,

		$ fitsheader image.fits
		$ fitsinfo image.fits

- To load different kernel e.g (python3, DESI master, DESI 19.12)
		$ source /global/common/software/desi/desi_environment.sh master
		$ source /global/common/software/desi/desi_environment.sh 19.12  

		In /global/common/software/desi/desi_environment.sh it is possible 
		to see the different kernels