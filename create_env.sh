#!/bin/bash

ENV_NAME=$1
PYTHON_VERSION=$2
KERNEL_NAME=$3


# Load the desi environment
source /global/common/software/desi/desi_environment.sh

# Load the conda base configuration
source ${DESICONDA}/etc/profile.d/conda.sh
conda activate

# Create the conda environment 
conda create -n $ENV_NAME --copy python==$PYTHON_VERSION ipykernel
source activate $ENV_NAME

# Create the folder for the kernel
~/.conda/envs/$ENV_NAME/bin/./ipython kernel install --user --name=$ENV_NAME

echo -e "{
    \"argv\": [  
    \"/global/homes/j/jfsuarez/.local/share/jupyter/kernels/"$ENV_NAME"/activate_"$ENV_NAME"_jupyter.sh\",
    \"master\",
    \"{connection_file}\" 
    ],
    \"display_name\":\"$KERNEL_NAME\",
    \"language\":\"python\"
}" > /global/homes/j/jfsuarez/.local/share/jupyter/kernels/$ENV_NAME/kernel.json
         
         
echo -e "#!/bin/bash
version=\$1
connection_file=\$2

source /global/common/software/desi/desi_environment.sh \${version}
export CONDA_ENVS_PATH=/global/homes/j/jfsuarez/.conda/envs/
source activate $ENV_NAME
export PYTHONPATH=/global/homes/j/jfsuarez/.conda/envs/$ENV_NAME/lib/python3.9/site-packages:\${PYTHONPATH}
exec python -m ipykernel -f \${connection_file}
" > /global/homes/j/jfsuarez/.local/share/jupyter/kernels/$ENV_NAME/activate_${ENV_NAME}_jupyter.sh
        
chmod u+x /global/homes/j/jfsuarez/.local/share/jupyter/kernels/$ENV_NAME/activate_${ENV_NAME}_jupyter.sh